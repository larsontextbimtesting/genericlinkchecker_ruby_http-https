require 'rubygems'
require 'xmlsimple'
require 'net/http' #use https or http depending on use- also see code below
require 'openssl'
require 'csv'

$brokenLinks = 0
timeInt = Time.now.to_i
fileoutName = "allLinksChecked_#{timeInt}.html" 
fileoutNameNotFound = "allLinksNotFound_#{timeInt}.html"
fixIncompleteUrls = true #use to handle links that start with things like "/protected/....."
inputCSV = "allBookMaterialLinksUnchanged.csv" #"allBookMaterialLinksUnchanged.csv" #"recheckLinks.csv"
startLine = 0

def CheckLink(origString, urlString, limit, line, fileout, fileoutNotFound)
	begin	
		url = URI.parse(urlString)
		http = Net::HTTP.new(url.host, url.port)
		if (urlString.index("https") == 0)	
			http.use_ssl = true
			http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		end
		request = Net::HTTP::Head.new(url.request_uri)
		request['Cookie'] = "loggedin=1,teacher=1"
		r = http.request(request)	
	rescue
		p "something went wrong with this request: #{$!.backtrace}  #{line}: #{urlString}"
		fileoutNotFound.puts "something went wrong with this request (rescued): | Line: #{line} |<a href ='#{origString}'>#{origString}</a>|<br/>"
		$brokenLinks = $brokenLinks + 1
		return
	end
	case r
		when Net::HTTPSuccess     then 
		print "#{line}, "
		when Net::HTTPRedirection then 
			#p "redirected  Line: #{line} #{origString}- > #{r['location']}"
			CheckLink(origString, r['location'], limit - 1, line, fileout, fileoutNotFound)
		when Net::HTTPNotFound
			$brokenLinks = $brokenLinks + 1
			fileoutNotFound.puts "failed checking -NOT FOUND - (#{r.code}) | Line: #{line} |<a href ='#{origString}'>#{origString}</a>| |<a href =#{urlString}>#{urlString}</a><br/>"
			p "failed checking -NOT FOUND - (#{r.code})- Line: #{line} | #{origString}"
		else
		$brokenLinks = $brokenLinks + 1
		fileoutNotFound.puts "failed checking (#{r.code})- | Line: #{line} |<a href ='#{origString}'>#{origString}</a>| |<a href =#{urlString}>#{urlString}</a><br/>"
	end
end
 

def main(inputCSV, fileoutName, fileoutNameNotFound, fixIncompleteUrls, startLine)
	fileout = File.open(fileoutName, "w")
	fileout.puts "<!DOCTYPE html><html><head><title>BookMaterials all Links</title></head><body>"
	fileoutNotFound = File.open(fileoutNameNotFound, "w")
	fileoutNotFound.puts "<!DOCTYPE html><html><head><title>BookMaterials all Links Not Found</title></head><body>"
	checkedLinks = 0
	CSV.foreach(inputCSV) do |row| #Note if using csv from database, quotes needed removed from some rows
		checkedLinks = checkedLinks + 1
		fullUrl = row[0].gsub(' ','%20')
		if (fixIncompleteUrls)
			if(fullUrl.index("//") == 0)
				fullUrl = "http:#{fullUrl}" 
			elsif (fullUrl.index("content") == 0)
				fullUrl = "http://static.bigideasmath.com/protected/#{fullUrl}"
			elsif ((fullUrl.index("/protected") == 0) && (!fullUrl.index(".php").nil?)) #php is not run on static server. Assumes .php does not occur in other instances
				fullUrl = "https://www.bigideasmath.com#{fullUrl}"
			elsif (fullUrl.index("/protected") == 0)
				fullUrl = "http://static.bigideasmath.com#{fullUrl}"
			elsif (fullUrl.index("/uploads") == 0)
				fullUrl = "https://www.bigideasmath.com#{fullUrl}"
			elsif (fullUrl.index("/external") == 0)
				fullUrl = "https://www.bigideasmath.com#{fullUrl}"		
			elsif (fullUrl.index("https") == 0)
				fullUrl = fullUrl.sub! 'https', 'http'	
			end
		end
		fullUrl = fullUrl.gsub! '//', '/' #just cleaning places like protected/content/dcs_fl2//a1/c01/ct/tp_01_ct.html
		fullUrl = fullUrl.sub! 'http:/', 'http://' 
		fileout.puts "Line: #{checkedLinks}: <a href ='#{fullUrl}'>#{fullUrl}</a><br/>"
		if checkedLinks < startLine
			next
		end
		begin
			CheckLink(fullUrl, fullUrl, 10, checkedLinks, fileout, fileoutNotFound)
		rescue
			$brokenLinks = $brokenLinks + 1
			p "failed checking (rescued)- Line: #{checkedLinks}: | <a href ='#{fullUrl}'>#{fullUrl}</a><br/>"
			fileoutNotFound.puts "failed checking (rescued): #{$!.backtrace}  | Line: #{checkedLinks} |<a href ='#{fullUrl}'>#{fullUrl}</a>|<br/>"
		end	
	end


	fileout.puts "<h2>Total Checked Links: #{checkedLinks}</h2>"
	fileout.puts "<h2>Total Broken Links: #{$brokenLinks}</h2>"
	fileout.puts "See <a href = 'AllBookMaterialsLinksNotFound.html'>AllBookMaterialsLinksNotFound.html</a> For Specific Info Related to Links without 200 code returns"
	fileout.puts "</body></html>"
	p "Total Checked Links: #{checkedLinks} File with links: #{fileoutName}"
	p "Total Broken Links: #{$brokenLinks} File with links: #{fileoutNameNotFound}"
	fileout.close
	fileoutNotFound.puts "<h2 style='color:blue'>Total Checked Links: #{checkedLinks}</h2>"
	fileoutNotFound.puts "<h2 style='color:red'>Total Broken Links: #{$brokenLinks}</h2>"
	fileoutNotFound.puts "</body></html>"
	fileoutNotFound.close
end 

main(inputCSV, fileoutName, fileoutNameNotFound, fixIncompleteUrls, startLine)
